// java script es 6 updates

// section

//exponents
//es 5 version

const firstNum = 8**2;
console.log(firstNum);

//es 6 version
const secondNum = Math.pow(8,2);
console.log(secondNum);

// template literals
//allows to write strings without using concatenate

let myName = "john";

//es 5 version

let message = 'hello ' + myName;

console.log(message);

//es 6 version
//using template literal
//using backticks (``)

message2 = `Hello ${myName}! Welcome to programming ${1+1}`;
console.log(`message using template literals: ${message2}`);

//multi line using template literals
const anotherMessage = `
${myName} attended a math competition.
he won it by solving a problem.
yo is this gonna print in two lines?
`
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;
console.log(`the interest rate on your savings account is : ${parseFloat(principal * interestRate)}`);

// array destructuring

const fullName = ["juan", 'dela', 'cruz'];

//es 5

console.log(`hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! its nice to meet you`)

//es6 destructuring

const [firstName, middleName, lastName] = fullName;
console.log(`hello ${firstName} ${middleName} ${lastName}`)

// [Section] Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variable
	-Allows us to name array elements with variables instead of using index numbers
	- helps with code readability
	- Syntax
		let/const [variableName1, variableName2, variableName3] = arrayName
*/

//arrow functions
// compact alternate syntax to traditional functions

const hello = () => {
    console.log("hello world");
}

hello();

const printName = (first, middle, last) => {
    console.log(`${first} ${middle} ${last}`);
}
printName("john", "d", "smith")


//implicit return statement
// there are instances when you omit return 
// this works because even without the return
// java script implicitly adds it for the result


const add = (x,y) => {x + y;}

let total = add(2,2); // kita ko na mali pag call ko nung function kakahiya hahaha
console.log(total);
console.log("hello:")


//default function argument value
const greet = (name = 'Username') => {
    return `Good morning ${name}`;
}
console.log(greet())

// [SECTION] Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;
*/
const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.middleName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(middleName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({ givenName, maidenName, familyName}) {
    console.log(`${ givenName } ${ maidenName } ${ familyName }`);
}

getFullName(person);
// pag diff name ganito pala
//let { firstName: fname, lastName: lname } = person;



// [SECTION] Class-Based Object Blueprints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
	- Syntax
		class className {
			constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

/*
	ES5 
	function Pokemon(name,level) {

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;
		}
*/


class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
/*
	- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
	- No arguments provided will create an object without any values assigned to it's properties
	- let/const variableName = new ClassName();
*/
// let myCar = new Car();

/*
	- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
	- It does not mean that it's properties cannot be changed/immutable
*/
const myCar = new Car();

console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// Creating/instantiating a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);
